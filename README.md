<p align="center">
  <h1 align="center">
    <img src="https://gitee.com/mirrors/Berry/raw/master/berry-logo.png" alt="Berry" width=272 height=128>
  </h1>
  <p align="center">The Berry Script Language.</p>
</p>

Original projects

[https://github.com/Skiars/berry](https://github.com/Skiars/berry) Berry 

[https://bitbucket.org/rmitton/tigr/src](https://bitbucket.org/rmitton/tigr/src) Tigr


## Additions & Changes

### Berry:

Tigr module added.

Berry added file.read_binary(size) where size is upto size of int.

### Tigr:

removed original font system and replaced with simpler code. 

tigrBlitF for x and y flipping 


Lessons for Tigr module. to make a game 










