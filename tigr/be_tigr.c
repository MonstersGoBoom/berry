#include "be_object.h"
#include "be_strlib.h"
#include "be_mem.h"
#include "be_sys.h"
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

#include "tigr.h"
#define tigr_grafx2_font_impl
#include "tigr_grafx2_font.h"

#define tigr_exblitter_impl
#include "tigr_blitter.h"
#if BE_USE_TIGR_MODULE
static TPixel tigr_pen;
TigrFont *tfont;

//	helper to create the tigr bmp class 

static int m_tigr_bmp(bvm *vm,Tigr *tig)
{
	static const bnfuncinfo members[] = {
			{ ".data"		, NULL },
			{ "width"	, NULL },
			{ "height"	, NULL },
			{ "mx"	, NULL },
			{ "my"	, NULL },
			{ "buttons"	, NULL },
			{ NULL, NULL }
	};

	//	push the class and add the members
	be_pushclass(vm, "Tigr", members);
	be_call(vm, 0);
  be_pushcomptr(vm, tig);
	be_setmember(vm, -2, ".data");

	be_pushint(vm,tig->w);
	be_setmember(vm, -3, "width");

	be_pushint(vm,tig->h);
	be_setmember(vm, -4, "height");

	be_pushint(vm,0);
	be_setmember(vm, -5, "mx");

	be_pushint(vm,0);
	be_setmember(vm, -6, "my");

	be_pushint(vm,0);
	be_setmember(vm, -7, "buttons");

	be_pop(vm, 6);

	be_return(vm);
}

static int m_tigr_clear(bvm *vm)
{
	be_getmember(vm, 1, ".data");

	if (be_iscomptr(vm, -1)) {
			Tigr *fh = be_tocomptr(vm, -1);
			tigrClear(fh,tigr_pen);
			be_return(vm);
	}
	be_return_nil(vm);
}

static int m_tigr_keydown(bvm *vm)
{
	be_getmember(vm, 1, ".data");
	if (be_iscomptr(vm, -1)) {
			Tigr *fh = be_tocomptr(vm, -1);

			if (be_isint(vm,2)) {
				be_pushint(vm, tigrKeyDown(fh,be_toint(vm,2)));
				be_return(vm);
			}
			else 
			{
				be_raise(vm, "tigr_error",be_pushfstring(vm, "no valid key ID"));
			}
	}
	be_pushbool(vm, true);
	be_return(vm);
}

static int m_tigr_keyheld(bvm *vm)
{
	be_getmember(vm, 1, ".data");
	if (be_iscomptr(vm, -1)) {
			Tigr *fh = be_tocomptr(vm, -1);

			if (be_isint(vm,2)) {
				be_pushint(vm, tigrKeyHeld(fh,be_toint(vm,2)));
				be_return(vm);
			}
			else 
			{
				be_raise(vm, "tigr_error",be_pushfstring(vm, "no valid key ID"));
			}
	}
	be_pushbool(vm, true);
	be_return(vm);
}

//	free tigr object

static int m_tigr_free(bvm *vm)
{
	be_getmember(vm, 1, ".data");
	if (be_iscomptr(vm, -1)) {
			Tigr *fh = be_tocomptr(vm, -1);
			tigrFree(fh);
	}
	be_return(vm);
}

//	return true if closed
static int m_tigr_closed(bvm *vm)
{
	be_getmember(vm, 1, ".data");
	if (be_iscomptr(vm, -1)) {
			Tigr *fh = be_tocomptr(vm, -1);
			be_pushbool(vm, tigrClosed(fh));
			be_return(vm);
	}
	be_pushbool(vm, true);
	be_return(vm);
}

//	update bitmap 
static int m_tigr_update(bvm *vm)
{
	be_getmember(vm, 1, ".data");
	if (be_iscomptr(vm, -1)) {
			Tigr *fh = be_tocomptr(vm, -1);
			tigrUpdate(fh);
			//	update this things mouse info 
			int x,y,b;
			tigrMouse(fh,&x,&y,&b);
			be_pushint(vm,x);
			be_setmember(vm, 1, "mx");
			be_pushint(vm,y);
			be_setmember(vm, 1, "my");
			be_pushint(vm,b);
			be_setmember(vm, 1, "buttons");
    	be_pop(vm, 3);
	}
	be_return_nil(vm);
}

//	wrap tigrLoadImage
static int m_tigr_loadpng(bvm *vm)
{
const char *name=NULL;	
Tigr *load=NULL;
  if (be_top(vm) >= 1 && be_isstring(vm, 1)) {
		name= be_tostring(vm, 1);
	}
	load = tigrLoadImage(name);
	if (load==NULL)
	{
		be_raise(vm, "tigr_error",be_pushfstring(vm, "cannot open file '%s'", name));
	}
	m_tigr_bmp(vm,load);
	be_return(vm);
}

//	wrap tigrSetPostFX

static int m_tigr_setpostfx(bvm *vm)
{
Tigr *dest;
float hblur,vblur,scanlines,contrast;
	dest = NULL;
	be_getmember(vm, 1, ".data");
	if (be_iscomptr(vm, -1))
		dest = be_tocomptr(vm, -1);

	hblur = be_toreal(vm,2);
	vblur = be_toreal(vm,3);
	scanlines = be_toreal(vm,4);
	contrast = be_toreal(vm,5);

	tigrSetPostFX(dest,(int)hblur,(int)vblur,scanlines,contrast);
  be_return_nil(vm);
}

//	wrap tigrGet
static int m_tigr_get(bvm *vm)
{
Tigr *dest;
float x,y;
	dest = NULL;
	be_getmember(vm, 1, ".data");
	if (be_iscomptr(vm, -1))
		dest = be_tocomptr(vm, -1);

	x = be_toreal(vm,2);
	y = be_toreal(vm,3);

	TPixel pix = tigrGet(dest,(int)x,(int)y);

	be_pushint(vm, 0xff000000 | (pix.r<<16) | (pix.g<<8) | pix.b);
	be_return(vm);
}
//	set tigr_pen 
//	either uint32 RGBA 
//	or 3 float RGB
//	or 4 float RGBA

static int m_tigr_setpen(bvm *vm)
{
  int argc = be_top(vm);
	if (argc==1)
	{
		uint32_t rgba = be_toint(vm,1);
		tigr_pen = tigrRGBA(rgba>>24,rgba>>16,rgba>>8,rgba);
	}
	else if (argc==3)
	{
		float r = be_toreal(vm,1);
		float g = be_toreal(vm,2);
		float b = be_toreal(vm,3);
		tigr_pen = tigrRGB(r,g,b);
	}
	else if (argc==4)
	{
		float r = be_toreal(vm,1);
		float g = be_toreal(vm,2);
		float b = be_toreal(vm,3);
		float a = be_toreal(vm,4);
		tigr_pen = tigrRGBA(r,g,b,a);
	}
  be_return_nil(vm);	
}

//	plot using the current pen 
static int m_tigr_plot(bvm *vm)
{
Tigr *dest;
float x,y;

	dest = NULL;

	be_getmember(vm, 1, ".data");
	if (be_iscomptr(vm, -1))
		dest = be_tocomptr(vm, -1);

	x = be_toreal(vm,2);
	y = be_toreal(vm,3);

	tigrPlot(dest,x,y,tigr_pen);
  be_return_nil(vm);
}

//	filled rectangle with current pen 
/*
void tigrRect(Tigr *bmp, int x, int y, int w, int h, TPixel color)
{
	int x1, y1;
	if (w <= 0 || h <= 0)
		return;
	
	x1 = x + w-1;
	y1 = y + h-1;
	tigrLine(bmp, x, y, x1, y, color);
	tigrLine(bmp, x1, y, x1, y1, color);
	tigrLine(bmp, x1, y1, x, y1, color);
	tigrLine(bmp, x, y1, x, y, color);
}
*/
static int m_tigr_filled_rect(bvm *vm)
{
Tigr *dest;
float x,y,w,h;

	dest = NULL;

	be_getmember(vm, 1, ".data");
	if (be_iscomptr(vm, -1))
		dest = be_tocomptr(vm, -1);

	x = be_toreal(vm,2);
	y = be_toreal(vm,3);
	w = be_toreal(vm,4);
	h = be_toreal(vm,5);

	for (int q=0;q<h;q++)
		tigrLine(dest,x,y+q,x+w,y+q,tigr_pen);
//	tigrFill(dest,x,y,w,h,tigr_pen);
  be_return_nil(vm);
}

//	hollow rect with current pen 
static int m_tigr_rect(bvm *vm)
{
Tigr *dest;
float x,y,w,h;

	dest = NULL;

	be_getmember(vm, 1, ".data");
	if (be_iscomptr(vm, -1))
		dest = be_tocomptr(vm, -1);

	x = be_toreal(vm,2);
	y = be_toreal(vm,3);
	w = be_toreal(vm,4);
	h = be_toreal(vm,5);

	tigrRect(dest,x,y,w,h,tigr_pen);
  be_return_nil(vm);
}

//	line with pen 
static int m_tigr_line(bvm *vm)
{
Tigr *dest;
float x0,y0,x1,y1;

	dest = NULL;

	be_getmember(vm, 1, ".data");
	if (be_iscomptr(vm, -1))
		dest = be_tocomptr(vm, -1);

	x0 = be_toreal(vm,2);
	y0 = be_toreal(vm,3);
	x1 = be_toreal(vm,4);
	y1 = be_toreal(vm,5);

	tigrLine(dest,x0,y0,x1,y1,tigr_pen);
  be_return_nil(vm);
}

//	print 
static int m_tigr_print(bvm *vm)
{
Tigr *dest;
float x,y;
	dest = NULL;

	be_getmember(vm, 1, ".data");
	if (be_iscomptr(vm, -1))
		dest = be_tocomptr(vm, -1);

	x = be_toreal(vm,2);
	y = be_toreal(vm,3);

	const char *name=NULL;	
  if (be_top(vm) >= 1 && be_isstring(vm, 4)) {
		name= be_tostring(vm, 4);
	}
	tigrPrintN(dest, tfont, (int)x,(int)y, tigr_pen,name);
  be_return_nil(vm);
}

/*
	m_tigr_blit 
	parse berry args and call tigrBlit 
 */

static int m_tigr_blit(bvm *vm)
{
Tigr *src,*dest;
float dx,dy,sx,sy,w,h,xf,yf;
	src = NULL;
	dest = NULL;

	be_getmember(vm, 1, ".data");
	if (be_iscomptr(vm, -1))
		dest = be_tocomptr(vm, -1);

	be_getmember(vm, 2, ".data");
	if (be_iscomptr(vm, -1))
		src = be_tocomptr(vm, -1);

	dx = be_toreal(vm,3);
	dy = be_toreal(vm,4);
	sx = be_toreal(vm,5);
	sy = be_toreal(vm,6);
	w = be_toreal(vm,7);
	h = be_toreal(vm,8);

	xf = be_toreal(vm,9);
	yf = be_toreal(vm,10);

	if (w==0.0f) w=dest->w;
	if (h==0.0f) h=dest->h;

	if ((src==NULL) || (dest==NULL))
	{
		be_raise(vm, "tigr_error",be_pushfstring(vm, "can't blit '%x' to '%x'", src,dest));
	}

	tigrBlitTintF(dest,src,(int)dx,(int)dy,(int)sx,(int)sy,(int)w,(int)h,tigr_pen,(int)(xf)&1,(int)(yf)&1);
  be_return_nil(vm);
}

//	create tigr window
static int m_tigr_window(bvm *vm)
{
int w,h,flag;
const char *name=NULL;

	w=320;
	h=240;
	flag = TIGR_FIXED;

	if (be_isint(vm,1)) {
		w = be_toint(vm,1);
	}
	if (be_isint(vm,2)) {
		h = be_toint(vm,2);
	}

  if (be_top(vm) >= 1 && be_isstring(vm, 3)) {
		name= be_tostring(vm, 3);
	}

	if (be_isint(vm,4)) {
		flag = be_toint(vm,4);
	}

	m_tigr_bmp(vm,tigrWindow(w,h,name,flag));


	tfont = tigrDefaultFont();

	be_return(vm);
}

// create a bitmap 
static int m_tigr_bitmap(bvm *vm)
{
int w,h;

	w=128;
	h=128;

	if (be_isint(vm,1)) {
		w = be_toint(vm,1);
	}
	if (be_isint(vm,2)) {
		h = be_toint(vm,2);
	}

	m_tigr_bmp(vm,tigrBitmap(w,h));
	be_return(vm);
}


#if !BE_USE_PRECOMPILED_OBJECT
be_native_module_attr_table(tigr) {
    be_native_module_function("Window", m_tigr_window),
    be_native_module_function("LoadPNG", m_tigr_loadpng),
    be_native_module_function("Blit", m_tigr_blit),
*/		
};

be_define_native_module(tigr, NULL);
#else
/* @const_object_info_begin
module tigr (scope: global, depend: BE_USE_TIGR_MODULE) {
		Window, func(m_tigr_window)
		Bitmap, func(m_tigr_bitmap)
		Free,		func(m_tigr_free)
		Closed, func(m_tigr_closed)
		Update, func(m_tigr_update)
		Print,	func(m_tigr_print)
		SetPostFX,func(m_tigr_setpostfx)
		Get,		func(m_tigr_get)
		SetPen,	func(m_tigr_setpen)
		Plot,		func(m_tigr_plot)
		Fill,		func(m_tigr_filled_rect)
		Rect,		func(m_tigr_rect)
		Line,		func(m_tigr_line)
		LoadPNG,func(m_tigr_loadpng)
		Blit, 	func(m_tigr_blit)
		Clear,	func(m_tigr_clear)
		KeyDown,func(m_tigr_keydown)
		KeyHeld,func(m_tigr_keyheld)
		//	consts
		TK_PAD0, int(TK_PAD0)
		TK_PAD1, int(TK_PAD1)
		TK_PAD2, int(TK_PAD2)
		TK_PAD3, int(TK_PAD3)
		TK_PAD4, int(TK_PAD4)
		TK_PAD5, int(TK_PAD5)
		TK_PAD6, int(TK_PAD6)
		TK_PAD7, int(TK_PAD7)
		TK_PAD8, int(TK_PAD8)
		TK_PAD9, int(TK_PAD9)
		TK_PADMUL, int(TK_PADMUL)
		TK_PADADD, int(TK_PADADD)
		TK_PADENTER, int(TK_PADENTER)
		TK_PADSUB, int(TK_PADSUB)
		TK_PADDOT, int(TK_PADDOT)
		TK_PADDIV, int(TK_PADDIV)
		TK_ESCAPE, int(TK_ESCAPE)
		TK_F1, int(TK_F1)
		TK_F2, int(TK_F2)
		TK_F3, int(TK_F3)
		TK_F4, int(TK_F4)
		TK_F5, int(TK_F5)
		TK_F6, int(TK_F6)
		TK_F7, int(TK_F7)
		TK_F8, int(TK_F8)
		TK_F9, int(TK_F9)
		TK_F10, int(TK_F10)
		TK_F11, int(TK_F11)
		TK_F12, int(TK_F12)
		TK_BACKSPACE, int(TK_BACKSPACE)
		TK_TAB,     	int(TK_TAB),     
		TK_RETURN,		int(TK_RETURN),
		TK_SHIFT,			int(TK_SHIFT),
		TK_CONTROL,		int(TK_CONTROL),
		TK_ALT,				int(TK_ALT),
		TK_PAUSE,			int(TK_PAUSE),
		TK_CAPSLOCK,	int(TK_CAPSLOCK),
		TK_ESCAPE,		int(TK_ESCAPE),
		TK_SPACE,			int(TK_SPACE),
		TK_PAGEUP,		int(TK_PAGEUP),
		TK_PAGEDN,		int(TK_PAGEDN),
		TK_END,				int(TK_END),
		TK_HOME,			int(TK_HOME),
		TK_LEFT,			int(TK_LEFT),
		TK_UP,				int(TK_UP),
		TK_RIGHT,			int(TK_RIGHT),
		TK_DOWN,			int(TK_DOWN),
		TK_INSERT,		int(TK_INSERT),
		TK_DELETE,		int(TK_DELETE),
		TK_LWIN,			int(TK_LWIN),
		TK_RWIN,			int(TK_RWIN),
		TK_NUMLOCK,		int(TK_NUMLOCK),
		TK_SCROLL,		int(TK_SCROLL),
		TK_LSHIFT,		int(TK_LSHIFT),
		TK_RSHIFT,		int(TK_RSHIFT),
		TK_LCONTROL,	int(TK_LCONTROL),
		TK_RCONTROL,	int(TK_RCONTROL),
		TK_LALT,			int(TK_LALT),
		TK_RALT,			int(TK_RALT),
		TK_SEMICOLON,	int(TK_SEMICOLON),
		TK_EQUALS,		int(TK_EQUALS),
		TK_COMMA,			int(TK_COMMA),
		TK_MINUS,			int(TK_MINUS),
		TK_DOT,				int(TK_DOT),
		TK_SLASH,			int(TK_SLASH),
		TK_BACKTICK,	int(TK_BACKTICK),
		TK_LSQUARE,		int(TK_LSQUARE),
		TK_BACKSLASH,	int(TK_BACKSLASH),
		TK_RSQUARE,		int(TK_RSQUARE),
		TK_TICK				int(TK_TICK)

		TIGR_FIXED,int(TIGR_FIXED)
		TIGR_AUTO,int(TIGR_AUTO)
		TIGR_2X,int(TIGR_2X)
		TIGR_3X,int(TIGR_3X)
		TIGR_4X,int(TIGR_4X)
}
@const_object_info_end */
#include "../generate/be_fixed_tigr.h"
#endif

#endif /* BE_USE_TIGR_MODULE */
