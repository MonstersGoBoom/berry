
#ifdef tigr_exblitter_impl
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#define EXPAND(X) ((X) + ((X) > 0))
#define CLIP0(X, X2, W) if (X < 0) { W += X; X2 -= X; X = 0; }
#define CLIP1(X, DW, W) if (X + W > DW) W = DW - X;
#define CLIP() \
	CLIP0(dx, sx, w);		\
	CLIP0(dy, sy, h);		\
	CLIP0(sx, dx, w);		\
	CLIP0(sy, dy, h);		\
	CLIP1(dx, dst->w, w);	\
	CLIP1(dy, dst->h, h);	\
	CLIP1(sx, src->w, w);	\
	CLIP1(sy, src->h, h);	\
	if (w <= 0 || h <= 0)	\
		return

void tigrBlitTintF(Tigr *dst, Tigr *src, int dx, int dy, int sx, int sy, int w, int h, TPixel tint,int xf,int yf)
{
	TPixel *td, *ts;
	int x, st, dt, xr,xg,xb,xa;
	int rx;
	CLIP();

	xr = EXPAND(tint.r);
	xg = EXPAND(tint.g);
	xb = EXPAND(tint.b);
	xa = EXPAND(tint.a);

	td = &dst->pix[dy*dst->w + dx];
	ts = &src->pix[sy*src->w + sx];
	st = src->w;
	if (yf==1)
	{
		ts = &src->pix[((sy+h)-1)*src->w + sx];
		st = -src->w;
	}
	dt = dst->w;
	do {
		for (x=0;x<w;x++)
		{
			rx = x;
			if (xf==1)
				rx = w-x-1;

			unsigned r = (xr * ts[rx].r) >> 8;
			unsigned g = (xg * ts[rx].g) >> 8;
			unsigned b = (xb * ts[rx].b) >> 8;
			unsigned a = xa * EXPAND(ts[rx].a);
			td[x].r += (unsigned char)((r - td[x].r)*a >> 16);
			td[x].g += (unsigned char)((g - td[x].g)*a >> 16);
			td[x].b += (unsigned char)((b - td[x].b)*a >> 16);
			td[x].a += (unsigned char)((ts[x].a - td[x].a)*a >> 16);
		}
		ts += st;
		td += dt;
	} while(--h);
}
#undef CLIP0
#undef CLIP1
#undef CLIP

#else 
void tigrBlitTintF(Tigr *dst, Tigr *src, int dx, int dy, int sx, int sy, int w, int h, TPixel tint,int xf,int yf);
#endif 

