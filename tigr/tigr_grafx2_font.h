
#ifdef tigr_grafx2_font_impl
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#include "font.h"

Tigr *tigrDropShadow(Tigr *bitmap,TPixel color)
{
	TPixel marker = tigrGet(bitmap,0,0);
	Tigr *spare = tigrBitmap(bitmap->w+1,bitmap->h+1);
	tigrBlitTint(spare,bitmap,1,2,0,1,bitmap->w-1,bitmap->h,color);
	tigrBlitTint(spare,bitmap,0,0,0,0,bitmap->w,bitmap->h,tigrRGB(0xff,0xff,0xff));
	tigrBlit(bitmap,spare,0,0,0,0,bitmap->w,bitmap->h);
	tigrFree(bitmap);
	return spare;
}


TigrFont *tigrProcessFont(Tigr *fbit)
{
	TigrFont *font = (TigrFont *)calloc(1, sizeof(TigrFont));
	if (font==NULL) { printf("Memory Allocation Failed\n");exit(0);}
	font->bitmap = fbit;
	font->numGlyphs = 128-32;
	font->glyphs = (TigrGlyph *)calloc(font->numGlyphs, sizeof(TigrGlyph));
	//	top line of font is markers 
	TPixel mark_color = tigrGet(fbit,0,0);
	//	scan bitmap 
	int chr = 0;
	int chrx = 1;
	for (int x=0;x<fbit->w;x++)
	{
		TPixel marker = tigrGet(fbit,x,0);
		//	check this pixel for the marker color 
		if ((marker.r == mark_color.r) && (marker.g == mark_color.g) && (marker.b == mark_color.b))
		{
			TigrGlyph *g;
			//	if we found a marker , this is the end of the current chr
			//	chrx is the left edge. x is the right edge

			g = &font->glyphs[chr];
			g->x = chrx;
			g->y = 1;
			g->h = fbit->h-1;
			g->w = x-chrx;
			g->code = chr+32; // ASCII
//			printf("char %c at %d,%d is %dx%d\n",g->code,g->x,g->y,g->w,g->h);

			chrx=x+1;
			chr++;
		}
	}
	//	often there's only upper case
	//	so duplicate the A-Z to a-z 
	if (chr<80)
	{
		for (int c=64;c<96;c++)
		{
				TigrGlyph *g,*s;
				g = &font->glyphs[c];
				s = &font->glyphs[c-32];
				g->x = s->x;
				g->y = s->y;
				g->h = s->h;
				g->w = s->w;
				g->code = c+32; // ASCII


		}
	}

//	printf("chars loaded %d\n",chr);

	return font;
}

static TigrGlyph *get(TigrFont *font, int code)
{
	unsigned lo = 0, hi = font->numGlyphs;
	while (lo < hi) {
		unsigned guess = (lo + hi) / 2;
		if (code < font->glyphs[guess].code) hi = guess;
		else lo = guess + 1;
	}

	if (lo == 0 || font->glyphs[lo-1].code != code)
		return &font->glyphs['?' - 32];
	else
		return &font->glyphs[lo-1];
}


void tigrSetupFont(TigrFont *font);

void tigrPrintN(Tigr *dest, TigrFont *font, int x, int y, TPixel color, const char *text, ...)
{
	char tmp[1024];
	TigrGlyph *g;
	va_list args;
	const char *p;
	int start = x, c;

	// Expand the formatting string.
	va_start(args, text);
	vsnprintf(tmp, sizeof(tmp), text, args);
	tmp[sizeof(tmp)-1] = 0;
	va_end(args);

	// Print each glyph.
	p = tmp;
	while (*p)
	{
		p = tigrDecodeUTF8(p, &c);
		if (c == '\r')
			continue;
		if (c == '\n') {
			x = start;
			y += 8;
			continue;
		}
		if (c == ' ')
		{
			x+=6;
			continue;
		}

		g = get(font, c);
		tigrBlitTint(dest, font->bitmap, x, y, g->x, g->y, g->w, g->h, color);
		x += g->w;
	}
}

TigrFont *tigrDefaultFont()
{
	Tigr *fbit = tigrLoadImageMem(small_tigr_font,small_tigr_font_size);
	return (tigrProcessFont(fbit));
}

TigrFont *tigrLoadFontN(const char *filename)
{
	Tigr *fbit = tigrLoadImage(filename);
	if (fbit==NULL) { printf("load %s failed\n",filename);exit(0);}
	return (tigrProcessFont(fbit));
}

#else 
TigrFont *tigrDefaultFont();
void tigrPrintN(Tigr *dest, TigrFont *font, int x, int y, TPixel color, const char *text, ...);
TigrFont *tigrLoadFontN(const char *filename);
#endif 

