#
# simply move an object around with keyboard 
#
import tigr
# open the window 320 by 240 pixels , 2X scale 
var screen = tigr.Window(320,240,"Lesson 1",tigr.TIGR_2X)
# simple red rectangle moved by keys
class Player
	var x,y
	def init(_x,_y)
		self.x=_x
		self.y=_y
	end
	def draw()
		# red rectangle
		# color order
		# RED 	ff......
		# GREEN ..00....
		# BLUE 	....00..
		# ALPHA ......ff
		tigr.SetPen(0xff0000ff)
		tigr.Fill(screen,self.x,self.y,16,16)
	end
	def move()
		# if holding right , add 2 to the position X 
		if (tigr.KeyHeld(screen,tigr.TK_RIGHT))
			self.x+=2	
		end 
		# if holding left , subtract 2 to the position X 
		if (tigr.KeyHeld(screen,tigr.TK_LEFT))
			self.x-=2
		end 
		# if holding down , add 2 to the position Y 
		if (tigr.KeyHeld(screen,tigr.TK_DOWN))
			self.y+=2
		end 
		# if holding up , subtract 2 to the position Y 
		if (tigr.KeyHeld(screen,tigr.TK_UP))
			self.y-=2
		end 
	end
end

# create new player
player=Player(32,32)
# main loop 
# check if the user closed the window OR pressed escape
while(!tigr.Closed(screen) && !tigr.KeyDown(screen,tigr.TK_ESCAPE))
	tigr.SetPen(0x00000000)
	tigr.Clear(screen)
	# do player logic
	player.move()
	player.draw()
	# always update
	tigr.Update(screen)
end 

