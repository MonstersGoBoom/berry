# Simple Player
class Player
	var x,y,xs,ys,world,grounded,frame,direction
	def init(_x,_y,_world)
		# copy position 
		self.grounded = false
		self.x=_x
		self.y=_y
		self.direction = 1
		self.frame = 0.0
		self.world = _world
		# set speed to 0 
		self.xs = 0.0 
		self.ys = 0.0
	end
	def draw()
		tigr.SetPen(0xffffffff)
		tu = (int(self.frame)&3)*16
		xf = 0
		if (self.direction==-1)
			xf = 1
		end
		tigr.Blit(screen,player_image,self.x,self.y,tu,0,16,16,xf)
	end

	def move()
		var lx,ly,lxs,lys
		lx = self.x 
		ly = self.y 
		# adjust movement speed
		if (tigr.KeyHeld(screen,tigr.TK_RIGHT))
			self.xs+=MOVE_SPEED
			if (self.xs>MAX_MOVE_SPEED)
				self.xs=MAX_MOVE_SPEED
			end
			self.direction = 1
			self.frame=time.clock()*6
		end 
		if (tigr.KeyHeld(screen,tigr.TK_LEFT))
			self.xs-=MOVE_SPEED
			if (self.xs<-MAX_MOVE_SPEED)
				self.xs=-MAX_MOVE_SPEED
			end
			self.direction = -1
			self.frame=time.clock()*6
		end 
		if ((tigr.KeyHeld(screen,tigr.TK_UP)) && (self.grounded==true))
			self.ys=JUMP_SPEED
		end 
		# x first
		# apply speed
		self.x += self.xs
		self.xs *= FRICTION
		for (o:0 .. self.world.entities.size()-1)
			var other = self.world.entities[o]
			if (other.type==NONSOLID)
				if (checkCollision(self,other))
					other.hit(self)
					break
				end
			end
			if (other.type==SOLID)
				if (checkCollision(self,other))
					if (self.x > lx)
						self.x = other.x - 16
					else 
						self.x = other.x + 16
					end 
					self.xs = 0
					break
				end
			end
		end
		# apply gravity
		if (self.ys<8)
			self.ys+= GRAVITY
		end
		self.y += self.ys

		# y collision
		# clear that we're grounded
		wasgrounded = self.grounded
		self.grounded=false

		for (o:0 .. self.world.entities.size()-1)
			var other = self.world.entities[o]
			if (other.type==NONSOLID)
				if (checkCollision(self,other))
					other.hit(self)
					break
				end
			end
			if (other.type==SOLID)
				if (checkCollision(self,other))
					if (self.y > ly)
						self.y = other.y - 16
						if (wasgrounded==false)
#							for (i:0 .. 100)
#								Particle(self.x+8,self.y+14)
#							end
						end
						self.grounded=true
					else 
						self.y = other.y + 16
					end 
					self.ys = 0
					break
				end
			end
		end
		
		# check against the edges 
		# bottom 
		if (self.y>screen.height-16)
			self.y = 0
		end
		# top 
		if (self.y<0)
			self.y = screen.height-16 
		end
		# right
		if (self.x>screen.width-16)
			self.x = 0
		end
		# left
		if (self.x<0)
			self.x = screen.width-16
		end
	end
end

return Player