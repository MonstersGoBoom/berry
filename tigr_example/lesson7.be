#
# external class to load a map and render
# extern class for player 
# 
 
import sys 
import os
import tigr
import math 
import time
import string

import "tigr_example/fmfmap.be" as fmfmap
import "tigr_example/player.be" as Player

GRAVITY = 0.2
MOVE_SPEED = 0.11
MAX_MOVE_SPEED = 1.5
JUMP_SPEED = -4.6
FRICTION = 0.96
NONSOLID = 0
SOLID = 1

# open the window
var screen = tigr.Window(256,240,"Lesson 7",tigr.TIGR_3X)
var tiles_image = tigr.LoadPNG("tigr_example/maptileset.png")
var player_image = tigr.LoadPNG("tigr_example/character-run.png")
var world 

def checkCollision(a,b)
  return ((a.x < b.x + 16) &&
          (a.x + 16 > b.x) &&
         	(a.y < b.y + 16) &&
         	(16 + a.y > b.y))
end

# basic block 
# only one type 

tileset_xwrap = tiles_image.width / 16
tileset_ywrap = tiles_image.height / 16

def blitTile(x,y,index)
	tu = (index % tileset_xwrap) * 16
	tv = math.floor((index /tileset_xwrap)) * 16
	tigr.Blit(screen,tiles_image,x,y,tu,tv,16,16)
end

class Block 
	var x,y,id,type,ishit
	def init(_x,_y,_id)
		self.x = _x 
		self.y = _y
		self.id = _id
		self.ishit=false
		self.type = SOLID
	end
	def draw()
#		tigr.Blit(screen,tiles_image,self.x,self.y,16,0,16,16)
#		tigr.Rect(screen,self.x,self.y,16,16)
	end
end

class Gem
	var x,y,id,type,ishit,index
	def init(_x,_y,_id)
		self.x = _x 
		self.y = _y
		self.id = _id
		self.type = NONSOLID
		self.ishit = false
	end
	def hit(ent)
		self.ishit = true
	end
	def draw()
		blitTile(self.x,self.y,self.id)
	end
end

var types = [nil,Block]
class World 
	var entities
	var tokill
	def init(_map,_width)
		self.tokill = []
		self.entities = []
	end
	def draw()
		tigr.SetPen(0xffffffff)
		self.tokill.clear()
		for (o:0 .. self.entities.size()-1)
			self.entities[o].draw()
			if (self.entities[o].ishit==true)
				self.tokill.push(o)
			end
		end

		for (i:0 .. self.tokill.size()-1)
			self.entities.remove(self.tokill[i])
		end
	end
end

world = World()
# decide what to do with this info 
# is it a collision 
# an entity ? 
# just draw a gem 

def isBlock(layer,index,x,y)
	if (layer==0)
		if ((index<32) && (index!=0))
			o = Block(x,y,index)
			world.entities.push( o )
		end
		return false
	end
	if (layer==1)
		if (index==67)
			o = Gem(x,y,index)
			o.index = world.entities.size()
			world.entities.push( o )
		end
	end
	return false
end

var fmap = fmfmap("tigr_example/levels256.fmf",tiles_image,isBlock)

# create world 
tigr.SetPostFX(screen,1,1,0,1.2)

player=Player(64.0,32.0,world)

while(!tigr.Closed(screen) && !tigr.KeyDown(screen,tigr.TK_ESCAPE))
	timer = time.clock()
	tigr.SetPen(0x00000000)
	tigr.Clear(screen)

	fmap.draw(0,0)
	world.draw()
	player.move()
	player.draw()
	tigr.Print(screen,4,16,string.format('Ms %.8f',(time.clock()-timer)*1000.))

	tigr.Update(screen)
end 

