#
# simply move an object around with keyboard 
# with simple inertia 
#
import tigr

# open the window
var screen = tigr.Window(320,240,"Lesson 2",tigr.TIGR_2X)

# simple red rectangle moved by keys
class Player
	var x,y,xs,ys
	def init(_x,_y)
		# copy position 
		self.x=_x
		self.y=_y
		# set speed to 0 
		self.xs = 0.0 
		self.ys = 0.0
	end
	def draw()
		# red rectangle
		tigr.SetPen(0xff0000ff)
		tigr.Fill(screen,self.x,self.y,16,16)
	end
	def move()
		# adjust movement speed
		if (tigr.KeyHeld(screen,tigr.TK_RIGHT))
			self.xs+=0.1
		end 
		if (tigr.KeyHeld(screen,tigr.TK_LEFT))
			self.xs-=0.1
		end 
		if (tigr.KeyHeld(screen,tigr.TK_UP))
			self.ys-=0.1
		end 
		if (tigr.KeyHeld(screen,tigr.TK_DOWN))
			self.ys+=0.1
		end 
		# apply speed
		self.x += self.xs
		self.y += self.ys

		# apply gravity
		if (self.ys<8)
			self.ys+=0.04
		end

		# check against the edges 
		# bottom 
		if (self.y>screen.height-16)
			self.y = screen.height-16 
			self.ys = 0 
		end
		# top 
		if (self.y<0)
			self.y = 0 
			self.ys = 0 
		end
		# right
		if (self.x>screen.width-16)
			self.x = screen.width-16 
			self.xs = 0 
		end
		# left
		if (self.x<0)
			self.x = 0 
			self.xs = 0 
		end
	end
end

# create new player
player=Player(32.0,32.0)
while(!tigr.Closed(screen) && !tigr.KeyDown(screen,tigr.TK_ESCAPE))
	tigr.SetPen(0x00000000)
	tigr.Clear(screen)

	# do it 
	player.move()
	player.draw()

	tigr.Update(screen)
end 

