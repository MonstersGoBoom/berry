#
# simply move an object around with keyboard 
# with simple inertia 
# added world 
#
import tigr
import math 

GRAVITY = 0.1
MOVE_SPEED = 0.11
JUMP_SPEED = -3.2
FRICTION = 0.98

# open the window
var screen = tigr.Window(256,240,"Lesson 3",tigr.TIGR_2X)

var map = [
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,
  1,1,0,0,1,1,1,1,1,1,1,1,0,0,1,1,
]

# assume 16x16
def checkCollision(a,b)
  return ((a.x < b.x + 16) &&
          (a.x + 16 > b.x) &&
         	(a.y < b.y + 16) &&
         	(16 + a.y > b.y))
end

class Block 
	var x,y,id 
	def init(_x,_y,_id)
		self.x = _x 
		self.y = _y
		self.id = _id
	end
	def draw()
		tigr.SetPen(0xffffffff)
		if (self.id == 0)
#			tigr.Rect(screen,self.x,self.y,16,16)
		else 
			tigr.Fill(screen,self.x,self.y,16,16)
		end
	end
end

class World 
	var map,width 
	var blocks 
	def init(_map,_width)
		self.map = _map
		self.width = _width
		self.blocks = []
		for (i:0 .. _map.size()-1)
			x = (i % _width) * 16
			y = math.floor(i / _width) * 16
			if (_map[i]!=0)
				self.blocks.push( Block(x,y,_map[i]))
			end
		end
	end

	def draw()
		for (o:0 .. self.blocks.size()-1)
			self.blocks[o].draw()
		end
	end
end

# simple red rectangle moved by keys
class Player
	var x,y,xs,ys,world,grounded
	def init(_x,_y,_world)
		# copy position 
		self.grounded = false
		self.x=_x
		self.y=_y
		self.world = _world
		# set speed to 0 
		self.xs = 0.0 
		self.ys = 0.0
	end
	def draw()
		# red rectangle
		if (self.grounded==false)
			tigr.SetPen(0xff0000ff)
		else 
			tigr.SetPen(0xffff00ff)
		end
		tigr.Fill(screen,self.x,self.y,16,16)
	end
	def move()
		var lx,ly,lxs,lys

		lx = self.x 
		ly = self.y 

		# adjust movement speed
		if (tigr.KeyHeld(screen,tigr.TK_RIGHT))
			self.xs+=MOVE_SPEED
		end 
		if (tigr.KeyHeld(screen,tigr.TK_LEFT))
			self.xs-=MOVE_SPEED
		end 
		if ((tigr.KeyHeld(screen,tigr.TK_UP)) && (self.grounded==true))
			self.ys=JUMP_SPEED
		end 
		# x first
		# apply speed
		self.x += self.xs
		self.xs *= FRICTION
		for (o:0 .. self.world.blocks.size()-1)
			var other = self.world.blocks[o]
			if (checkCollision(self,other))
				if (self.x > lx)
					self.x = other.x - 16
				else 
					self.x = other.x + 16
				end 
				self.xs = 0
				break
			end
		end

		# apply gravity
		if (self.ys<8)
			self.ys+= GRAVITY
		end
		self.y += self.ys

		# y collision
		# clear that we're grounded
		self.grounded=false

		for (o:0 .. self.world.blocks.size()-1)
			var other = self.world.blocks[o]
			if (checkCollision(self,other))
				if (self.y > ly)
					self.y = other.y - 16
					self.grounded=true
				else 
					self.y = other.y + 16
				end 
				self.ys = 0
				break
			end
		end
		
		# check against the edges 
		# bottom 
		if (self.y>screen.height-16)
			self.y = 0
		end
		# top 
		if (self.y<0)
			self.y = screen.height-16 
		end
		# right
		if (self.x>screen.width-16)
			self.x = 0
		end
		# left
		if (self.x<0)
			self.x = screen.width-16
		end
	end
end

# create world 
world = World(map,16)

# create new player
player=Player(32.0,32.0,world)
while(!tigr.Closed(screen) && !tigr.KeyDown(screen,tigr.TK_ESCAPE))
	tigr.SetPen(0x00000000)
	tigr.Clear(screen)

	# draw map 
	world.draw()

	# do it 
	player.move()
	player.draw()

	tigr.Update(screen)
end 

