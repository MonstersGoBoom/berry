import tigr
import time
import math
import string

var squinkle = tigr.LoadPNG("tigr_example/demo.png")
var screen = tigr.Window(320,240,"Hello",tigr.TIGR_2X)

tigr.SetPostFX(screen,1,1,1.0,2.0)

##########################################
# simple Vector type
##########################################
class Vector 
	var x,y
	def init(_x,_y)
		self.x=_x
		self.y=_y
	end

	def tostring()
		return "Vector (" + string.format('%.3f %.3f', self.x,self.y) + ")"
	end

	def +(other)
		if ((type(other)=="instance"))
			return( Vector( self.x + other.x,self.y + other.y) )
		else
			return( Vector( self.x + other,self.y + other) )
		end
	end

	def -(other)
		if ((type(other)=="instance"))
			return( Vector( self.x - other.x,self.y - other.y) )
		else
			return( Vector( self.x - other,self.y - other) )
		end
	end

	def /(other)
		if ((type(other)=="instance"))
			return( Vector( self.x / other.x,self.y / other.y) )
		else
			return( Vector( self.x / other,self.y / other) )
		end
	end

	def *(other)
		if ((type(other)=="instance"))
			return( Vector( self.x * other.x,self.y * other.y) )
		else
			return( Vector( self.x * other,self.y * other) )
		end
	end
end

# test
v = Vector(32,12)
print(v)
c = v * 4
print(c)
print(v)

Particles = []
Particles.init()

# simple Particle
class Particle
	var pos,velocity,i,life
	def init(x,y)
		self.pos = Vector(x,y)
		self.velocity = Vector( ((math.rand()%100)/100.0)-0.5,((math.rand()%100)/100.0)-0.5)
		self.i = Particles.size()
		self.life = 255
		Particles.push(self)
	end
	def draw()
		tigr.Blit(screen,squinkle,self.pos.x-8,self.pos.y-16,0,0,16,16)
#		self.life = self.life - 1 
		self.pos = self.pos + self.velocity
		self.velocity.y = self.velocity.y + .1
		if (self.pos.x<0) self.velocity.x=-self.velocity.x end
		if (self.pos.x>screen.width) self.velocity.x=-self.velocity.x end
		if (self.pos.y>screen.height)
			self.velocity.y=-self.velocity.y
		end
	end
end
# spawn some particles
for (i : 0 .. 3000)
	p=Particle(math.rand()%screen.width,math.rand()%screen.height)
end
var rgba,timer
while(!tigr.Closed(screen) && !tigr.KeyDown(screen,tigr.TK_ESCAPE))
	tigr.SetPen(0x00000000)
	tigr.Clear(screen)
	tigr.SetPen(0xffffff80)
	timer = time.clock()
	if (screen.buttons!=0)
		for (i:0 .. Particles.size()-1)
				Particles[i].draw()
		end
	end
	tigr.SetPen(0x80ffc0c0)
	tigr.Fill(screen,32,32,64,64)
	tigr.Rect(screen,64,32,64,64)
	rgba = tigr.Get(screen,screen.mx,screen.my)
	tigr.SetPen(rgba)
	tigr.Blit(screen,squinkle,screen.mx,screen.my,0,0,128,128)
	tigr.SetPen(0xffffff80)
	tigr.Line(screen,0,0,256,256)
	tigr.Print(screen,0,0,string.format('%.8x %.3f',rgba,screen.mx))
	tigr.Print(screen,0,16,string.format('ms %.8f',(time.clock()-timer)*1000.))
	tigr.Update(screen)
end 

