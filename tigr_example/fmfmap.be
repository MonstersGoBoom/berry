import sys 
import os
import string

class FMFmap
	var width,height,parts_width,parts_height,nlayers,bpt,layers
	var tilesimage
	var mapimage
	var touched
	def init(filename,tilesimage,hascollision)
		self.tilesimage = tilesimage
		var f = open(filename,"rb")
		f.read_binary(4)
		f.read_binary(4)
		self.width = f.read_binary(4)
		self.height = f.read_binary(4)
		self.parts_width = f.read_binary(1)
		self.parts_height = f.read_binary(1)
		self.nlayers = f.read_binary(1)
		self.bpt = f.read_binary(1)
		self.touched = true
		var rsize = 1

		if (self.bpt==16)
			rsize=2
		end

		
		# create an image for it 
		self.mapimage = tigr.Bitmap(self.width*self.parts_width,self.height*self.parts_height)
		self.layers = []
		print(self.layers)
		for (i:0 .. self.nlayers-1)
			layer = []
			for (q:0 .. ((self.width*self.height)-1))
				index = f.read_binary(rsize)
				tx = (q % self.width) * self.parts_width
				ty = math.floor(q / self.width) * self.parts_height
				if (hascollision!=nil)
					if (hascollision(i,index,tx,ty))
					end
				end
				layer.push(index)
			end
			self.layers.push(layer)
		end
		for (i:0 .. self.layers.size()-1)
			arr = self.layers[i]
#			print(arr[i])
		end
		f.close()
	end
	def drawTo(x,y)
		tigr.SetPen(0xffffffff)
		tileset_xwrap = self.tilesimage.width / self.parts_width
		tileset_ywrap = self.tilesimage.height / self.parts_height
#		for (l:0 .. self.layers.size()-1)
		l = 0
			layer = self.layers[l]
			for (i:0 .. layer.size()-1)
				index = layer[i]
				tx = (i % self.width) * self.parts_width
				ty = math.floor(i / self.width) * self.parts_height
				tu = (index % tileset_xwrap) * self.parts_width
				tv = math.floor((index /tileset_xwrap)) * self.parts_height
				tigr.Blit(self.mapimage,self.tilesimage,tx,ty,tu,tv,16,16)
			end
#		end
	end
	def draw()
		if (self.touched==true)
			self.drawTo(0,0)
			self.touched=false
		end
		tigr.SetPen(0xffffffff)
		tigr.Blit(screen,self.mapimage,0,0,0,0)
	end
end

return FMFmap



